package uk.co.techblue.accountshome.exception;

import uk.co.techblue.govtalk.dto.GovTalkMessage;



public class GatewayServiceException extends Exception {

	private static final long serialVersionUID = -508229939957195555L;

	private GovTalkMessage errorResponse;

	public GatewayServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public GatewayServiceException(String message) {
		super(message);
	}

	public GatewayServiceException(Throwable cause) {
		super(cause);
	}

	public GovTalkMessage getErrorResponse() {
		return errorResponse;
	}

	public void setErrorResponse(GovTalkMessage errorResponse) {
		this.errorResponse = errorResponse;
	}

}
