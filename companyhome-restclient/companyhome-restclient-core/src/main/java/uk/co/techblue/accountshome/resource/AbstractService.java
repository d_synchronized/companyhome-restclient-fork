package uk.co.techblue.accountshome.resource;

import uk.co.techblue.client.Resource;
import uk.co.techblue.client.Service;

public abstract class AbstractService<RT extends Resource> extends Service<RT>{

	public AbstractService(String restBaseUri) {
		super(restBaseUri);
	}

}
