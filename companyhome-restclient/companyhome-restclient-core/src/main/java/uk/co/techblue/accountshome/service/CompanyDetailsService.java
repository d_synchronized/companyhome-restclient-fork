package uk.co.techblue.accountshome.service;

import org.jboss.resteasy.client.ClientResponse;



import uk.co.techblue.accountshome.exception.GatewayServiceException;
import uk.co.techblue.accountshome.resource.AbstractService;
import uk.co.techblue.accountshome.resource.CompanyDetailsResource;
import uk.co.techblue.govtalk.dto.GovTalkMessage;
import uk.co.techblue.govtalk.dto.request.GenericGovTalkMessageRequest;
import uk.co.techblue.govtalk.dto.responses.CompanyDetailsResponse;



public class CompanyDetailsService extends AbstractService<CompanyDetailsResource>{

	public CompanyDetailsService(String restBaseUri) {
		super(restBaseUri);
	}

	@Override
	protected Class<CompanyDetailsResource> getResourceClass() {
		return CompanyDetailsResource.class;
	}
	
	public CompanyDetailsResponse getCompanyDetailsFromGateway(GenericGovTalkMessageRequest govTalkMessage) throws GatewayServiceException{
		ClientResponse<CompanyDetailsResponse> clientResponse =resourceProxy.getCompanyDetails(govTalkMessage);
		return parseEntityFromResponse(clientResponse, GatewayServiceException.class);		
	}

}
