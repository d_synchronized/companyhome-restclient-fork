package uk.co.techblue.accountshome.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import org.jboss.resteasy.client.ClientResponse;



import uk.co.techblue.client.Resource;
import uk.co.techblue.govtalk.dto.GovTalkMessage;
import uk.co.techblue.govtalk.dto.request.GenericGovTalkMessageRequest;
import uk.co.techblue.govtalk.dto.responses.CompanyDetailsResponse;



@Path("http://xmlgw.companieshouse.gov.uk/v1-0/xmlgw/Gateway")
public interface CompanyDetailsResource extends Resource{

	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	ClientResponse<CompanyDetailsResponse> getCompanyDetails(GenericGovTalkMessageRequest govTalkMessage);
	
}
