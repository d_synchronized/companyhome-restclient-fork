package uk.co.techblue.client;

import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.ProxyFactory;

import uk.co.techblue.accountshome.exception.GatewayServiceException;

public abstract class Service<RT extends Resource> {

	protected final String restBaseUri;

	protected final RT resourceProxy;

	public Service(final String restBaseUri) {
		this.restBaseUri = restBaseUri;
		this.resourceProxy = getResourceProxy(getResourceClass(), restBaseUri);
	}

	protected abstract Class<RT> getResourceClass();

	protected <T> T getResourceProxy(final Class<T> clazz,
			final String serverUri) {
		return getClientService(clazz, serverUri);
	}

	private final <T> T getClientService(final Class<T> clazz,
			final String serverUri) {
		return ProxyFactory.create(clazz, serverUri);
	}

	protected <T, EX extends GatewayServiceException> T parseEntityFromResponse(
			ClientResponse<T> clientResponse, Class<EX> exceptionClaZZ)
			throws EX {
		T entity = null;
		try {
			entity = clientResponse.getEntity();
		} finally {
			clientResponse.releaseConnection();
		}
		return entity;

	}

}
