package uk.co.techblue;

import java.io.File;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import uk.co.techblue.accountshome.exception.GatewayServiceException;
import uk.co.techblue.accountshome.service.CompanyDetailsService;
import uk.co.techblue.companydetail.dto.CompanyDetails;
import uk.co.techblue.companydetail.dto.CompanyDetailsRequest;
import uk.co.techblue.govtalk.dto.Body;
import uk.co.techblue.govtalk.dto.GovTalkDetails;
import uk.co.techblue.govtalk.dto.GovTalkMessage;
import uk.co.techblue.govtalk.dto.Header;
import uk.co.techblue.govtalk.dto.IDAuthentication;
import uk.co.techblue.govtalk.dto.Keys;
import uk.co.techblue.govtalk.dto.MessageDetails;
import uk.co.techblue.govtalk.dto.SenderDetails;
import uk.co.techblue.govtalk.dto.request.GenericGovTalkMessageRequest;
import uk.co.techblue.govtalk.dto.responses.CompanyDetailsResponse;

/**
 * Unit test for simple App.
 */
public class AppTest {
	static GenericGovTalkMessageRequest govTalkMessage;

	public static void main(String args[]) {
		govTalkMessage = new GenericGovTalkMessageRequest();
		govTalkMessage.setEnvelopeVersion("1.0");

		Header header = new Header();

		MessageDetails messageDetails = new MessageDetails();
		messageDetails.setClazz("CompanyDetails");
		messageDetails.setQualifier("request");
		messageDetails.setTransactionID("14456561");
		header.setMessageDetails(messageDetails);

		SenderDetails senderDetails = new SenderDetails();
		IDAuthentication idAuthentication = new IDAuthentication();
		idAuthentication.setSenderID("XMLGatewayTestUserID");

		IDAuthentication.Authentication authentication = new IDAuthentication.Authentication();
		authentication.setMethod("MD5SIGN");
		authentication.setValue("a54b01eee93758db1db3ed05ea9e2df8");

		idAuthentication.getAuthentication().add(authentication);

		senderDetails.setEmailAddress("dishant.anand@mail.techblue.co.uk");
		header.setSenderDetails(senderDetails);
		senderDetails.setIDAuthentication(idAuthentication);

		govTalkMessage.setHeader(header);

		GovTalkDetails govTalkDetails = new GovTalkDetails();
		Keys keys = new Keys();
		govTalkDetails.setKeys(keys);
		govTalkMessage.setGovTalkDetails(govTalkDetails);

		Body body = new Body();
		CompanyDetailsRequest companyDetailsRequest = new CompanyDetailsRequest();
		companyDetailsRequest.setCompanyNumber("03176906");
		companyDetailsRequest.setGiveMortTotals(true);

		body.getAny().add(companyDetailsRequest);
		govTalkMessage.setBody(body);

//		sendRequest();
		
//		try {
//			convertfromXmlToJava();
//		} catch (JAXBException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}

		CompanyDetailsService companyDetailsService = new CompanyDetailsService(
				"http://xmlgw.companieshouse.gov.uk/v1-0/xmlgw/Gateway");
		try {
			CompanyDetailsResponse xml = companyDetailsService
					.getCompanyDetailsFromGateway(govTalkMessage);

			System.out.println(xml);
			// CompanyDetails
			// companyDetails=(CompanyDetails)govTalkMessage.getBody().getAny().get(0);
		} catch (GatewayServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void sendRequest() {

		String result;
		StringWriter sw = new StringWriter();
		try {
			JAXBContext carContext = JAXBContext.newInstance(
					GenericGovTalkMessageRequest.class, CompanyDetailsRequest.class);
			Marshaller carMarshaller = carContext.createMarshaller();
			carMarshaller.marshal(govTalkMessage, sw);
			result = sw.toString();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
		System.out.println(result);

	}

	public static void convertfromXmlToJava() throws JAXBException {

		JAXBContext jaxbContext = JAXBContext.newInstance(CompanyDetailsResponse.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
		File XMLfile = new File("D://Barborn-Referencing//accountshome//src//main//java//com//barbon//referencing//accountshome//NameSearchRequest.xml");

		CompanyDetailsResponse countryIndia = (CompanyDetailsResponse) jaxbUnmarshaller.unmarshal(XMLfile);
		
//		System.out.println(countryIndia);


	}

}
