package uk.co.techblue.govtalk.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "messageDetails",
    "senderDetails"
})
public class Header {

    @XmlElement(name = "MessageDetails", required = true)
    protected MessageDetails messageDetails;
    @XmlElement(name = "SenderDetails")
    protected SenderDetails senderDetails;

    /**
     * Gets the value of the messageDetails property.
     * 
     * @return
     *     possible object is
     *     {@link GovTalkMessage.Header.MessageDetails }
     *     
     */
    public MessageDetails getMessageDetails() {
        return messageDetails;
    }

    /**
     * Sets the value of the messageDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link GovTalkMessage.Header.MessageDetails }
     *     
     */
    public void setMessageDetails(MessageDetails value) {
        this.messageDetails = value;
    }

    /**
     * Gets the value of the senderDetails property.
     * 
     * @return
     *     possible object is
     *     {@link GovTalkMessage.Header.SenderDetails }
     *     
     */
    public SenderDetails getSenderDetails() {
        return senderDetails;
    }

    /**
     * Sets the value of the senderDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link GovTalkMessage.Header.SenderDetails }
     *     
     */
    public void setSenderDetails(SenderDetails value) {
        this.senderDetails = value;
    }
}

