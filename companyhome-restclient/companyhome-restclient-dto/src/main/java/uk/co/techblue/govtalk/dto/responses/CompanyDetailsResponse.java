package uk.co.techblue.govtalk.dto.responses;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import uk.co.techblue.companydetail.dto.CompanyDetailsRequest;
import uk.co.techblue.govtalk.dto.GovTalkMessage;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ CompanyDetailsRequest.class })
@XmlType(name = "", propOrder = { "envelopeVersion", "header",
		"govTalkDetails", "body" })
@XmlRootElement(name = "GovTalkMessage")
public class CompanyDetailsResponse extends GovTalkMessage{

	@XmlElement(name = "Body")
	private CompanyDetailBody body;

	public CompanyDetailBody getBody() {
		return body;
	}

	public void setBody(CompanyDetailBody body) {
		this.body = body;
	}



}
