package uk.co.techblue.govtalk.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "processed",
    "result"
})
public class GatewayValidation {

    @XmlElement(name = "Processed", required = true)
    protected String processed;
    @XmlElement(name = "Result", required = true)
    protected String result;

    /**
     * Gets the value of the processed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessed() {
        return processed;
    }

    /**
     * Sets the value of the processed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessed(String value) {
        this.processed = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResult(String value) {
        this.result = value;
    }

}

