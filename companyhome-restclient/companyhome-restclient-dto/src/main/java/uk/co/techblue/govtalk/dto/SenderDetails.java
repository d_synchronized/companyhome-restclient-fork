package uk.co.techblue.govtalk.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idAuthentication",
    "x509Certificate",
    "emailAddress"
})
public class SenderDetails {

    @XmlElement(name = "IDAuthentication")
    protected IDAuthentication idAuthentication;
    @XmlElement(name = "X509Certificate")
    protected byte[] x509Certificate;
    @XmlElement(name = "EmailAddress")
    protected String emailAddress;

    /**
     * Gets the value of the idAuthentication property.
     * 
     * @return
     *     possible object is
     *     {@link IDAuthentication }
     *     
     */
    public IDAuthentication getIDAuthentication() {
        return idAuthentication;
    }

    /**
     * Sets the value of the idAuthentication property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDAuthentication }
     *     
     */
    public void setIDAuthentication(IDAuthentication value) {
        this.idAuthentication = value;
    }

    /**
     * Gets the value of the x509Certificate property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getX509Certificate() {
        return x509Certificate;
    }

    /**
     * Sets the value of the x509Certificate property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setX509Certificate(byte[] value) {
        this.x509Certificate = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

}