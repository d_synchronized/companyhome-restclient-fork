package uk.co.techblue.govtalk.dto.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import uk.co.techblue.companydetail.dto.CompanyDetailsRequest;
import uk.co.techblue.govtalk.dto.Body;
import uk.co.techblue.govtalk.dto.GovTalkMessage;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ CompanyDetailsRequest.class ,GovTalkMessage.class})
@XmlType(name = "", propOrder = { "envelopeVersion", "header",
		"govTalkDetails", "body" })
@XmlRootElement(name = "GovTalkMessage")
public class GenericGovTalkMessageRequest extends GovTalkMessage {

	@XmlElement(name = "Body")
	protected Body body;

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

}
