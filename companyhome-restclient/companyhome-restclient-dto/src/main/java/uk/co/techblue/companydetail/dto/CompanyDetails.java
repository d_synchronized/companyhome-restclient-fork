package uk.co.techblue.companydetail.dto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "companyName", "companyNumber", "regAddress",
		"companyCategory", "companyStatus", "countryOfOrigin",
		"registrationDate", "dissolutionDate", "incorporationDate",
		"closureDate", "previousNames", "accounts", "returns", "mortgages",
		"sicCodes", "lastFullMemDate", "lastBulkShareDate", "weededDate",
		"hasBranchInfo", "hasAppointments", "inLiquidation",
		"limitedPartnerships" })
@XmlRootElement(name = "CompanyDetails",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
public class CompanyDetails {

	@XmlElement(name = "CompanyName", required = true,namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected String companyName;
	@XmlElement(name = "CompanyNumber", required = true,namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected String companyNumber;
	@XmlElement(name = "RegAddress", required = true,namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected RegAddressType regAddress;
	@XmlElement(name = "CompanyCategory", required = true,namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected String companyCategory;
	@XmlElement(name = "CompanyStatus", required = true,namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected String companyStatus;
	@XmlElement(name = "CountryOfOrigin", required = true,namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected String countryOfOrigin;
	@XmlElement(name = "RegistrationDate",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected XMLGregorianCalendar registrationDate;
	@XmlElement(name = "DissolutionDate",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected XMLGregorianCalendar dissolutionDate;
	@XmlElement(name = "IncorporationDate",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected XMLGregorianCalendar incorporationDate;
	@XmlElement(name = "ClosureDate",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected XMLGregorianCalendar closureDate;
	@XmlElement(name = "PreviousNames",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected List<PreviousNamesType> previousNames;
	@XmlElement(name = "Accounts", required = true,namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected Accounts accounts;
	@XmlElement(name = "Returns", required = true,namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected Returns returns;
	@XmlElement(name = "Mortgages",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected Mortgages mortgages;
	@XmlElement(name = "SICCodes", required = true,namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected SicCodesType sicCodes;
	@XmlElement(name = "LastFullMemDate",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected XMLGregorianCalendar lastFullMemDate;
	@XmlElement(name = "LastBulkShareDate",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected XMLGregorianCalendar lastBulkShareDate;
	@XmlElement(name = "WeededDate",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	@XmlSchemaType(name = "date",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected XMLGregorianCalendar weededDate;
	@XmlElement(name = "HasBranchInfo",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected boolean hasBranchInfo;
	@XmlElement(name = "HasAppointments",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected boolean hasAppointments;
	@XmlElement(name = "InLiquidation",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected boolean inLiquidation;
	@XmlElement(name = "LimitedPartnerships",namespace="http://xmlgw.companieshouse.gov.uk/v1-0/schema")
	protected LimitedPartnerships limitedPartnerships;

	/**
	 * Gets the value of the companyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the value of the companyName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyName(String value) {
		this.companyName = value;
	}

	/**
	 * Gets the value of the companyNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyNumber() {
		return companyNumber;
	}

	/**
	 * Sets the value of the companyNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyNumber(String value) {
		this.companyNumber = value;
	}

	/**
	 * Gets the value of the regAddress property.
	 * 
	 * @return possible object is {@link RegAddressType }
	 * 
	 */
	public RegAddressType getRegAddress() {
		return regAddress;
	}

	/**
	 * Sets the value of the regAddress property.
	 * 
	 * @param value
	 *            allowed object is {@link RegAddressType }
	 * 
	 */
	public void setRegAddress(RegAddressType value) {
		this.regAddress = value;
	}

	/**
	 * Gets the value of the companyCategory property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyCategory() {
		return companyCategory;
	}

	/**
	 * Sets the value of the companyCategory property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyCategory(String value) {
		this.companyCategory = value;
	}

	/**
	 * Gets the value of the companyStatus property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyStatus() {
		return companyStatus;
	}

	/**
	 * Sets the value of the companyStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyStatus(String value) {
		this.companyStatus = value;
	}

	/**
	 * Gets the value of the countryOfOrigin property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}

	/**
	 * Sets the value of the countryOfOrigin property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCountryOfOrigin(String value) {
		this.countryOfOrigin = value;
	}

	/**
	 * Gets the value of the registrationDate property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getRegistrationDate() {
		return registrationDate;
	}

	/**
	 * Sets the value of the registrationDate property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setRegistrationDate(XMLGregorianCalendar value) {
		this.registrationDate = value;
	}

	/**
	 * Gets the value of the dissolutionDate property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getDissolutionDate() {
		return dissolutionDate;
	}

	/**
	 * Sets the value of the dissolutionDate property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setDissolutionDate(XMLGregorianCalendar value) {
		this.dissolutionDate = value;
	}

	/**
	 * Gets the value of the incorporationDate property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getIncorporationDate() {
		return incorporationDate;
	}

	/**
	 * Sets the value of the incorporationDate property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setIncorporationDate(XMLGregorianCalendar value) {
		this.incorporationDate = value;
	}

	/**
	 * Gets the value of the closureDate property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getClosureDate() {
		return closureDate;
	}

	/**
	 * Sets the value of the closureDate property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setClosureDate(XMLGregorianCalendar value) {
		this.closureDate = value;
	}

	/**
	 * Gets the value of the previousNames property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the previousNames property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getPreviousNames().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link PreviousNamesType }
	 * 
	 * 
	 */
	public List<PreviousNamesType> getPreviousNames() {
		if (previousNames == null) {
			previousNames = new ArrayList<PreviousNamesType>();
		}
		return this.previousNames;
	}

	/**
	 * Gets the value of the accounts property.
	 * 
	 * @return possible object is {@link CompanyDetails.Accounts }
	 * 
	 */
	public Accounts getAccounts() {
		return accounts;
	}

	/**
	 * Sets the value of the accounts property.
	 * 
	 * @param value
	 *            allowed object is {@link CompanyDetails.Accounts }
	 * 
	 */
	public void setAccounts(Accounts value) {
		this.accounts = value;
	}

	/**
	 * Gets the value of the returns property.
	 * 
	 * @return possible object is {@link CompanyDetails.Returns }
	 * 
	 */
	public Returns getReturns() {
		return returns;
	}

	/**
	 * Sets the value of the returns property.
	 * 
	 * @param value
	 *            allowed object is {@link CompanyDetails.Returns }
	 * 
	 */
	public void setReturns(Returns value) {
		this.returns = value;
	}

	/**
	 * Gets the value of the mortgages property.
	 * 
	 * @return possible object is {@link CompanyDetails.Mortgages }
	 * 
	 */
	public Mortgages getMortgages() {
		return mortgages;
	}

	/**
	 * Sets the value of the mortgages property.
	 * 
	 * @param value
	 *            allowed object is {@link CompanyDetails.Mortgages }
	 * 
	 */
	public void setMortgages(Mortgages value) {
		this.mortgages = value;
	}

	/**
	 * Gets the value of the sicCodes property.
	 * 
	 * @return possible object is {@link SicCodesType }
	 * 
	 */
	public SicCodesType getSICCodes() {
		return sicCodes;
	}

	/**
	 * Sets the value of the sicCodes property.
	 * 
	 * @param value
	 *            allowed object is {@link SicCodesType }
	 * 
	 */
	public void setSICCodes(SicCodesType value) {
		this.sicCodes = value;
	}

	/**
	 * Gets the value of the lastFullMemDate property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getLastFullMemDate() {
		return lastFullMemDate;
	}

	/**
	 * Sets the value of the lastFullMemDate property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setLastFullMemDate(XMLGregorianCalendar value) {
		this.lastFullMemDate = value;
	}

	/**
	 * Gets the value of the lastBulkShareDate property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getLastBulkShareDate() {
		return lastBulkShareDate;
	}

	/**
	 * Sets the value of the lastBulkShareDate property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setLastBulkShareDate(XMLGregorianCalendar value) {
		this.lastBulkShareDate = value;
	}

	/**
	 * Gets the value of the weededDate property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getWeededDate() {
		return weededDate;
	}

	/**
	 * Sets the value of the weededDate property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setWeededDate(XMLGregorianCalendar value) {
		this.weededDate = value;
	}

	/**
	 * Gets the value of the hasBranchInfo property.
	 * 
	 */
	public boolean isHasBranchInfo() {
		return hasBranchInfo;
	}

	/**
	 * Sets the value of the hasBranchInfo property.
	 * 
	 */
	public void setHasBranchInfo(boolean value) {
		this.hasBranchInfo = value;
	}

	/**
	 * Gets the value of the hasAppointments property.
	 * 
	 */
	public boolean isHasAppointments() {
		return hasAppointments;
	}

	/**
	 * Sets the value of the hasAppointments property.
	 * 
	 */
	public void setHasAppointments(boolean value) {
		this.hasAppointments = value;
	}

	/**
	 * Gets the value of the inLiquidation property.
	 * 
	 */
	public boolean isInLiquidation() {
		return inLiquidation;
	}

	/**
	 * Sets the value of the inLiquidation property.
	 * 
	 */
	public void setInLiquidation(boolean value) {
		this.inLiquidation = value;
	}

	/**
	 * Gets the value of the limitedPartnerships property.
	 * 
	 * @return possible object is {@link CompanyDetails.LimitedPartnerships }
	 * 
	 */
	public LimitedPartnerships getLimitedPartnerships() {
		return limitedPartnerships;
	}

	/**
	 * Sets the value of the limitedPartnerships property.
	 * 
	 * @param value
	 *            allowed object is {@link CompanyDetails.LimitedPartnerships }
	 * 
	 */
	public void setLimitedPartnerships(LimitedPartnerships value) {
		this.limitedPartnerships = value;
	}

}
