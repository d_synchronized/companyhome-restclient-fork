package uk.co.techblue.companydetail.dto;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mortgageInd",
    "numMortCharges",
    "numMortOutstanding",
    "numMortPartSatisfied",
    "numMortSatisfied"
})
public class Mortgages {

    @XmlElement(name = "MortgageInd", required = true)
    protected String mortgageInd;
    @XmlElement(name = "NumMortCharges", required = true)
    protected BigInteger numMortCharges;
    @XmlElement(name = "NumMortOutstanding", required = true)
    protected BigInteger numMortOutstanding;
    @XmlElement(name = "NumMortPartSatisfied", required = true)
    protected BigInteger numMortPartSatisfied;
    @XmlElement(name = "NumMortSatisfied", required = true)
    protected BigInteger numMortSatisfied;

    /**
     * Gets the value of the mortgageInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMortgageInd() {
        return mortgageInd;
    }

    /**
     * Sets the value of the mortgageInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMortgageInd(String value) {
        this.mortgageInd = value;
    }

    /**
     * Gets the value of the numMortCharges property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumMortCharges() {
        return numMortCharges;
    }

    /**
     * Sets the value of the numMortCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumMortCharges(BigInteger value) {
        this.numMortCharges = value;
    }

    /**
     * Gets the value of the numMortOutstanding property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumMortOutstanding() {
        return numMortOutstanding;
    }

    /**
     * Sets the value of the numMortOutstanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumMortOutstanding(BigInteger value) {
        this.numMortOutstanding = value;
    }

    /**
     * Gets the value of the numMortPartSatisfied property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumMortPartSatisfied() {
        return numMortPartSatisfied;
    }

    /**
     * Sets the value of the numMortPartSatisfied property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumMortPartSatisfied(BigInteger value) {
        this.numMortPartSatisfied = value;
    }

    /**
     * Gets the value of the numMortSatisfied property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumMortSatisfied() {
        return numMortSatisfied;
    }

    /**
     * Sets the value of the numMortSatisfied property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumMortSatisfied(BigInteger value) {
        this.numMortSatisfied = value;
    }

}

