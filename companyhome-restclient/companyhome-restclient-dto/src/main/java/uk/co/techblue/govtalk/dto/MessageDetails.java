package uk.co.techblue.govtalk.dto;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "clazz",
    "qualifier",
    "function",
    "transactionID",
    "auditID",
    "correlationID",
    "responseEndPoint",
    "transformation",
    "gatewayTest",
    "gatewayTimestamp"
})
public class MessageDetails {

    @XmlElement(name = "Class", required = true)
    protected String clazz;
    @XmlElement(name = "Qualifier", required = true)
    protected String qualifier;
    @XmlElement(name = "Function")
    protected String function;
    @XmlElement(name = "TransactionID")
    protected String transactionID;
    @XmlElement(name = "AuditID")
    protected String auditID;
    @XmlElement(name = "CorrelationID")
    protected String correlationID;
    @XmlElement(name = "ResponseEndPoint")
    protected ResponseEndPoint responseEndPoint;
    @XmlElement(name = "Transformation")
    protected String transformation;
    @XmlElement(name = "GatewayTest")
    protected BigInteger gatewayTest;
    @XmlElement(name = "GatewayTimestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar gatewayTimestamp;

    /**
     * Gets the value of the clazz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * Sets the value of the clazz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClazz(String value) {
        this.clazz = value;
    }

    /**
     * Gets the value of the qualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualifier() {
        return qualifier;
    }

    /**
     * Sets the value of the qualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualifier(String value) {
        this.qualifier = value;
    }

    /**
     * Gets the value of the function property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFunction() {
        return function;
    }

    /**
     * Sets the value of the function property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFunction(String value) {
        this.function = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the auditID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuditID() {
        return auditID;
    }

    /**
     * Sets the value of the auditID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuditID(String value) {
        this.auditID = value;
    }

    /**
     * Gets the value of the correlationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Sets the value of the correlationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * Gets the value of the responseEndPoint property.
     * 
     * @return
     *     possible object is
     *     {@link GovTalkMessage.Header.MessageDetails.ResponseEndPoint }
     *     
     */
    public ResponseEndPoint getResponseEndPoint() {
        return responseEndPoint;
    }

    /**
     * Sets the value of the responseEndPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link GovTalkMessage.Header.MessageDetails.ResponseEndPoint }
     *     
     */
    public void setResponseEndPoint(ResponseEndPoint value) {
        this.responseEndPoint = value;
    }

    /**
     * Gets the value of the transformation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransformation() {
        return transformation;
    }

    /**
     * Sets the value of the transformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransformation(String value) {
        this.transformation = value;
    }

    /**
     * Gets the value of the gatewayTest property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getGatewayTest() {
        return gatewayTest;
    }

    /**
     * Sets the value of the gatewayTest property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setGatewayTest(BigInteger value) {
        this.gatewayTest = value;
    }

    /**
     * Gets the value of the gatewayTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGatewayTimestamp() {
        return gatewayTimestamp;
    }

    /**
     * Sets the value of the gatewayTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGatewayTimestamp(XMLGregorianCalendar value) {
        this.gatewayTimestamp = value;
    }
}


