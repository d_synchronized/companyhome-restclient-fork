package uk.co.techblue.companydetail.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "nextDueDate", "overdue", "lastMadeUpDate",
		"documentAvailable" })
public class Returns {

	@XmlElement(name = "NextDueDate")
	protected XMLGregorianCalendar nextDueDate;
	@XmlElement(name = "Overdue", required = true)
	protected String overdue;
	@XmlElement(name = "LastMadeUpDate")
	protected XMLGregorianCalendar lastMadeUpDate;
	@XmlElement(name = "DocumentAvailable")
	protected boolean documentAvailable;

	/**
	 * Gets the value of the nextDueDate property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getNextDueDate() {
		return nextDueDate;
	}

	/**
	 * Sets the value of the nextDueDate property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setNextDueDate(XMLGregorianCalendar value) {
		this.nextDueDate = value;
	}

	/**
	 * Gets the value of the overdue property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getOverdue() {
		return overdue;
	}

	/**
	 * Sets the value of the overdue property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setOverdue(String value) {
		this.overdue = value;
	}

	/**
	 * Gets the value of the lastMadeUpDate property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getLastMadeUpDate() {
		return lastMadeUpDate;
	}

	/**
	 * Sets the value of the lastMadeUpDate property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setLastMadeUpDate(XMLGregorianCalendar value) {
		this.lastMadeUpDate = value;
	}

	/**
	 * Gets the value of the documentAvailable property.
	 * 
	 */
	public boolean isDocumentAvailable() {
		return documentAvailable;
	}

	/**
	 * Sets the value of the documentAvailable property.
	 * 
	 */
	public void setDocumentAvailable(boolean value) {
		this.documentAvailable = value;
	}

}
