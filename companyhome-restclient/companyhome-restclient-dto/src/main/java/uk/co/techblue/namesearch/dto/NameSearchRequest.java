package uk.co.techblue.namesearch.dto;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "companyName",
    "dataSet",
    "sameAs",
    "searchRows",
    "continuationKey",
    "regressionKey"
})
@XmlRootElement(name = "NameSearchRequest")
public class NameSearchRequest {

    @XmlElement(name = "CompanyName", required = true)
    protected String companyName;
    @XmlElement(name = "DataSet", required = true)
    protected String dataSet;
    @XmlElement(name = "SameAs")
    protected Boolean sameAs;
    @XmlElement(name = "SearchRows")
    protected BigInteger searchRows;
    @XmlElement(name = "ContinuationKey")
    protected String continuationKey;
    @XmlElement(name = "RegressionKey")
    protected String regressionKey;

    /**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the dataSet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataSet() {
        return dataSet;
    }

    /**
     * Sets the value of the dataSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataSet(String value) {
        this.dataSet = value;
    }

    /**
     * Gets the value of the sameAs property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSameAs() {
        return sameAs;
    }

    /**
     * Sets the value of the sameAs property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSameAs(Boolean value) {
        this.sameAs = value;
    }

    /**
     * Gets the value of the searchRows property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSearchRows() {
        return searchRows;
    }

    /**
     * Sets the value of the searchRows property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSearchRows(BigInteger value) {
        this.searchRows = value;
    }

    /**
     * Gets the value of the continuationKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContinuationKey() {
        return continuationKey;
    }

    /**
     * Sets the value of the continuationKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContinuationKey(String value) {
        this.continuationKey = value;
    }

    /**
     * Gets the value of the regressionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegressionKey() {
        return regressionKey;
    }

    /**
     * Sets the value of the regressionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegressionKey(String value) {
        this.regressionKey = value;
    }

}
