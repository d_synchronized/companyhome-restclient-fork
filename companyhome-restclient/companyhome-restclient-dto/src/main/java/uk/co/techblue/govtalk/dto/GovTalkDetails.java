package uk.co.techblue.govtalk.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "keys", "targetDetails", "gatewayValidation",
		"channelRouting", "govTalkErrors", "gatewayAdditions" })
public class GovTalkDetails {

	@XmlElement(name = "Keys")
	protected Keys keys;
	@XmlElement(name = "TargetDetails")
	protected TargetDetails targetDetails;
	@XmlElement(name = "GatewayValidation")
	protected GatewayValidation gatewayValidation;
	@XmlElement(name = "ChannelRouting")
	protected List<ChannelRouting> channelRouting;
	@XmlElement(name = "GovTalkErrors")
	protected GovTalkErrors govTalkErrors;
	@XmlElement(name = "GatewayAdditions")
	protected GatewayAdditions gatewayAdditions;

	/**
	 * Gets the value of the keys property.
	 * 
	 * @return possible object is {@link GovTalkMessage.GovTalkDetails.Keys }
	 * 
	 */
	public Keys getKeys() {
		return keys;
	}

	/**
	 * Sets the value of the keys property.
	 * 
	 * @param value
	 *            allowed object is {@link GovTalkMessage.GovTalkDetails.Keys }
	 * 
	 */
	public void setKeys(Keys value) {
		this.keys = value;
	}

	/**
	 * Gets the value of the targetDetails property.
	 * 
	 * @return possible object is
	 *         {@link GovTalkMessage.GovTalkDetails.TargetDetails }
	 * 
	 */
	public TargetDetails getTargetDetails() {
		return targetDetails;
	}

	/**
	 * Sets the value of the targetDetails property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link GovTalkMessage.GovTalkDetails.TargetDetails }
	 * 
	 */
	public void setTargetDetails(TargetDetails value) {
		this.targetDetails = value;
	}

	/**
	 * Gets the value of the gatewayValidation property.
	 * 
	 * @return possible object is
	 *         {@link GovTalkMessage.GovTalkDetails.GatewayValidation }
	 * 
	 */
	public GatewayValidation getGatewayValidation() {
		return gatewayValidation;
	}

	/**
	 * Sets the value of the gatewayValidation property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link GovTalkMessage.GovTalkDetails.GatewayValidation }
	 * 
	 */
	public void setGatewayValidation(GatewayValidation value) {
		this.gatewayValidation = value;
	}

	/**
	 * Gets the value of the channelRouting property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the channelRouting property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getChannelRouting().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link GovTalkMessage.GovTalkDetails.ChannelRouting }
	 * 
	 * 
	 */
	public List<ChannelRouting> getChannelRouting() {
		if (channelRouting == null) {
			channelRouting = new ArrayList<ChannelRouting>();
		}
		return this.channelRouting;
	}

	/**
	 * Gets the value of the govTalkErrors property.
	 * 
	 * @return possible object is
	 *         {@link GovTalkMessage.GovTalkDetails.GovTalkErrors }
	 * 
	 */
	public GovTalkErrors getGovTalkErrors() {
		return govTalkErrors;
	}

	/**
	 * Sets the value of the govTalkErrors property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link GovTalkMessage.GovTalkDetails.GovTalkErrors }
	 * 
	 */
	public void setGovTalkErrors(GovTalkErrors value) {
		this.govTalkErrors = value;
	}

	/**
	 * Gets the value of the gatewayAdditions property.
	 * 
	 * @return possible object is
	 *         {@link GovTalkMessage.GovTalkDetails.GatewayAdditions }
	 * 
	 */
	public GatewayAdditions getGatewayAdditions() {
		return gatewayAdditions;
	}

	/**
	 * Sets the value of the gatewayAdditions property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link GovTalkMessage.GovTalkDetails.GatewayAdditions }
	 * 
	 */
	public void setGatewayAdditions(GatewayAdditions value) {
		this.gatewayAdditions = value;
	}
}