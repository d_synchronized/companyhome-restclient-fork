package uk.co.techblue.companydetail.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "title", "surname", "dob", "postTown",
		"postcode", "countrOfResidence", "personID" })
@XmlRootElement(name = "OfficerSearchItem")
public class OfficerSearchItem {

	@XmlElement(name = "Title", required = true)
	protected String title;
	@XmlElement(name = "Surname", required = true)
	protected String surname;
	@XmlElement(name = "DOB", required = true)
	@XmlSchemaType(name = "date")
	protected XMLGregorianCalendar dob;
	@XmlElement(name = "PostTown", required = true)
	protected String postTown;
	@XmlElement(name = "Postcode", required = true)
	protected String postcode;
	@XmlElement(name = "CountrOfResidence")
	protected String countrOfResidence;
	@XmlElement(name = "PersonID", required = true)
	protected String personID;

	/**
	 * Gets the value of the title property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the value of the title property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setTitle(String value) {
		this.title = value;
	}

	/**
	 * Gets the value of the surname property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Sets the value of the surname property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSurname(String value) {
		this.surname = value;
	}

	/**
	 * Gets the value of the dob property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 * 
	 */
	public XMLGregorianCalendar getDOB() {
		return dob;
	}

	/**
	 * Sets the value of the dob property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 * 
	 */
	public void setDOB(XMLGregorianCalendar value) {
		this.dob = value;
	}

	/**
	 * Gets the value of the postTown property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPostTown() {
		return postTown;
	}

	/**
	 * Sets the value of the postTown property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPostTown(String value) {
		this.postTown = value;
	}

	/**
	 * Gets the value of the postcode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * Sets the value of the postcode property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPostcode(String value) {
		this.postcode = value;
	}

	/**
	 * Gets the value of the countrOfResidence property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCountrOfResidence() {
		return countrOfResidence;
	}

	/**
	 * Sets the value of the countrOfResidence property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCountrOfResidence(String value) {
		this.countrOfResidence = value;
	}

	/**
	 * Gets the value of the personID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPersonID() {
		return personID;
	}

	/**
	 * Sets the value of the personID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPersonID(String value) {
		this.personID = value;
	}

}
